/*
 * The MIT License
 *
 * Copyright 2015 bjculkin.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.bjc.gates;

import java.util.Map;

/**
 * A descriptor of a logic gate, detailing any relevant info.
 *  This info includes
 *      <ul>
 *          <li> Title of this gate </li>
 *          <li> Description of this gate </li>
 *          <li> Number of slots, and details of each individual slot </li>
 *          <li> Any other notes about this gate </li>
 *      </ul>
 * @author bjculkin
 */
public interface IGateDescriptor {
    /**
     * Gets the title of this gate.
     *  The title of a gate describes its purpose at a glance.
     * @return The title of this gate.
     */
    String getTitle();
    
    /**
     * Gets the description of this gate.
     *  The description of a gate provides full details of how to use this gate
     *  in any way that it is intended to be used.
     * @return The description of this gate.
     */
    String getDescription();
    
    /**
     * Gets a map of all of the slots on this gate.
     *  The map contains both input & output slots, as well as the absolute slot
     *  number for each of them.
     * @return A map mapping slot numbers to descriptions of the attached slot. 
     */
    Map<Integer, ISlotDescriptor> getSlots();
    
    /**
     * Gets the notes for this gate.
     *  The notes for a gate are non-essential information about it, such as
     *  interesting examples, or historical info.
     * @return The notes for this gate.
     */
    String getNotes();
}
