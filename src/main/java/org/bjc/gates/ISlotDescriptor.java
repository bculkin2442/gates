/*
 * The MIT License
 *
 * Copyright 2015 bjculkin.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.bjc.gates;

/**
 * A descriptor of a slot on a logic gate.
 *  Provides a title, description, and
 *  any necessary notes for a particular slot, as well as input/output status.
 *
 * @author bjculkin
 */
public interface ISlotDescriptor {
    /**
     * Valid input/output modes for slots
     */
    public enum IOMode {
 
        /**
         * INPUT mode - This slot can only be used for input
         */
        INPUT, 
        /**
         * OUTPUT mode - This slot can only be used for output
         */
        OUTPUT
    }
    
    /**
     * Gets the title of this slot.
     *  The title is simply a descriptive name for the slot.
     * @return The title of this slot.
     */
    String getTitle();

    /**
     * Gets the description of this slot.
     *  The description describes the purpose of this slot, and any other
     *  information necessary to be able to set/reference it.
     * @return The description of this slot.
     */
    String getDescription();

    /**
     * Gets the notes for this slot.
     *  The notes for a slot are side information - not necessarily needed to
     *  successfully use the slot, but interesting/relevant to it in some way.
     * @return The notes for this slot.
     */
    String getNotes();
    
    /**
     * Gets the IO mode of this slot.
     *  The IO mode for a slot describes the valid ways for the specified slot
     *  to be used.
     * @return The IO mode for this slot.
     */
    IOMode getMode();
}
