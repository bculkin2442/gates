/*
 * The MIT License
 *
 * Copyright 2015 bjculkin.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.bjc.gates;

/**
 * A logic gate with a flexible number of inputs/outputs. The gates work fairly
 * simply. First, the outputs are wired up to lead to either sinks or other
 * gates, then whenever the output the current gates input is connected to
 * changes, that gate will signal this one that one of its inputs has changed.
 *
 * @author bjculkin
 */
public interface IGate {

    /**
     * Gets a descriptor describing this gate.
     *  The returned descriptor is assumed to be for all gates of a specific
     *  kind, not for once specific gate.
     * @return A descriptor describing the gate.
     */
    IGateDescriptor getDescriptor();
    
    /**
     * Polls the current state of this gate.
     *  Getting the state of the gate should not influence the internal state
     *  of the gate.
     * @return The current state of the gate.
     */
    boolean getState();

    /**
     * Signal the gate one of its inputs has changed.
     *  Signaling the gate of a slot change should cause something to happen
     *  if the change is different from the gates internal state, but should not
     *  cause a change if the change matches the gates internal state.
     * 
     * @param inputNo The slot that has changed.
     * @param state The new state of the slot.
     * @throws IndexOutOfBoundsException If the specified slot is not a valid
     *          slot.
     */
    void signal(int inputNo, boolean state) throws IndexOutOfBoundsException;

    /**
     * Connect an output of this gate to a input of another.
     *  This causes the specified gates input to be signaled, whenever a signal
     *  to this gate cause the specified output to change.
     * @param outputNo The output slot of this gate to wire to the gate.
     * @param gate The gate who will be signaled upon output change.
     * @param inputNo The slot of the specified gate to signal.
     * @throws IndexOutOfBoundsException If either of the specified slots is
     *          not a valid slot.
     */
    void wireOutput(int outputNo, IGate gate, int inputNo)
            throws IndexOutOfBoundsException;
}
